//
//  PageViewController.swift
//  onboarding
//
//  Created by Delia Cavalli on 22/11/2019.
//  Copyright © 2019 Delia Cavalli. All rights reserved.
//

import UIKit

// UIPageViewController: gestisce la navigazione tra le pagine di contenuto, in cui ogni pagina è gestita da una view controller figlia

// UIViewControllerDelegate: questo metodo consente di ricevere una notifica quando l'orientamento del dispositivo cambia e quando l'utente passa a una nuova pagina (...)

// UIViewControllerDataSource: questo protocollo è adottato da un oggetto che fornisce view controller alla page view controller in base alle necessità, in risposta ai gesti di navigazione

class PageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    // lazy var: viene usata perchè la variabile viene richiamata una sola volta
    
    // return: rimandami il dato con le modifiche avvenute
    
    // self: queste caratteristiche sono assegnate all'oggetto stesso
 
    lazy var orderedViewControllers: [UIViewController] = {
        return [self.newVc(viewController: "sbRed" ),
                self.newVc(viewController: "sbBlue"),
                self.newVc(viewController: "sbGreen")]
    }()
    
    // UIPageControl: un controllo che visualizza una serie orizzontale di punti, ognuno dei quali corrisponde a una pagina nel documento dell'app o altra entità del modello di dati.
    
    // override: il metodo di override è una funzione di linguaggio in cui una classe può fornire l'implementazione di un metodo già fornito da una delle sue classi principali. L'implementazione in questa classe sostituisce (questo è l'override [trad. sostituzione]) l'implementazione nella classe genitore.
    
    // viewDidLoad(): è chiamata dopo che la view controller è stata caricata in memoria
    
    var pageControl = UIPageControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataSource = self
        self.delegate = self
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
        self.delegate = self
        configurePageControl()
        
    }
    
    // configurePageControl: caratteristiche come colore e posizione dei dots
    
    func configurePageControl() {
        pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY - 70, width: UIScreen.main.bounds.width, height: 50))
        pageControl.numberOfPages = orderedViewControllers.count
        pageControl.currentPage = 0
        pageControl.tintColor = UIColor.black
        pageControl.pageIndicatorTintColor = UIColor.white
        pageControl.currentPageIndicatorTintColor = UIColor.black
        self.view.addSubview(pageControl)
    }
    
    // ?? dovrebbe essere la view controller bianca sulla quale arrivo dopo aver premuto il pulsante X
    
    func newVc (viewController: String) -> UIViewController {
        print("peppe")
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: viewController)
}

    // funzione usata per bloccare la prima view e non far scrollare le pagine all'infinito
    
    func pageViewController(_ _pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let previewsIndex = viewControllerIndex - 1
        guard previewsIndex >= 0 else {
//            return orderedViewControllers.last
            return nil
        }
        
        guard orderedViewControllers.count > previewsIndex else {
            return nil
            
        }
        return orderedViewControllers[previewsIndex]
        
    }
    
    // funzione usata per bloccare l'ultima view e non far scrollare le pagine all'infinito
       
       func pageViewController(_ _pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
           
           guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
               return nil
           }
           
           let previewsIndex = viewControllerIndex + 1
           guard previewsIndex >= 0 else {
               return orderedViewControllers.last
               //return nil
           }
           
           guard orderedViewControllers.count > previewsIndex else {
               return nil
               
           }
           return orderedViewControllers[previewsIndex]
           
       }
    
    // animazione per tornare sul tutorial, è una transizione
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = orderedViewControllers.firstIndex(of: pageContentViewController)!
    }
    
    }

